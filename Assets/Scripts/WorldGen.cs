﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using myEnums;

public class WorldGen : MonoBehaviour
{
    [SerializeField] Palette _palette;
    public Palette NowPalette { get { return _palette; } }
    [SerializeField] WorldGenData _worldGenData;
    public WorldGenData WorldGenData { get { return _worldGenData; } }

    [SerializeField] Material _matPlainBg;
    [SerializeField] Material _matWall;
    [SerializeField] Material _matFloor;

    public static float FinishZPos = 900f;
    [SerializeField] Transform _platform;
    [SerializeField] GameObject _spikes;
    [SerializeField] GameObject _wall;
    [SerializeField] GameObject _finish;
    [SerializeField] GameObject _pin;
    [SerializeField] Transform _50procLevel;
    [SerializeField] float _posWall = 2.496f;
    float _oneStepWall = 0.75f;

    Transform _worldParent;

    List<GameObject> _presetsL = new List<GameObject>();
    List<GameObject> _presetsR = new List<GameObject>();

    List<GameObject> _downedPins = new List<GameObject>();
    public GameObject DownedPins { set { _downedPins.Add(value); } }
    public void ResetDownedPins()
    {
        if (_downedPins.Count > 0)
            foreach (GameObject go in _downedPins)
                go.SetActive(true);
        _downedPins.Clear();
    }


    void Awake ()
    {
        if (Time.time < .1f)
            _worldGenData.ResetWorldData();

        _matWall.color = _palette.GetNowWallColor;
        _matFloor.color = _palette.GetNowPlatformColor;
        Camera.main.backgroundColor = _palette.GetNowSkyColors;
        _matPlainBg.color = _palette.GetNowSkyColors;


        _worldParent = GameObject.FindGameObjectWithTag("world").transform;

        //gen first wall
        _presetsL.Add(Instantiate(_wall, _worldParent));
        _presetsL[_presetsL.Count - 1].transform.localPosition = new Vector3(-_posWall, -8f, 0);
        _presetsL[_presetsL.Count - 1].transform.localScale = new Vector3(1, 7, 1);

        _presetsR.Add(Instantiate(_wall, _worldParent));
        _presetsR[_presetsR.Count - 1].transform.localPosition = new Vector3(_posWall, -8f, 0);
        _presetsR[_presetsR.Count - 1].transform.localScale = new Vector3(1, 7, 1);
        _presetsR[_presetsR.Count - 1].transform.GetChild(1).gameObject.layer = 9;

        //gen other wall
        for (int i = _worldGenData.SizeWorld; i > 0; i--)
        {
            GenPreset(ref _presetsL);
            GenPreset(ref _presetsR);
        }
        if (_presetsL[_presetsL.Count - 1].transform.GetChild(0).transform.position.z > _presetsR[_presetsR.Count - 1].transform.GetChild(0).transform.position.z)
        {
            FinishZPos = _presetsL[_presetsL.Count - 1].transform.GetChild(0).transform.position.z + 1f;
        }
        else
        {
            FinishZPos = _presetsR[_presetsR.Count - 1].transform.GetChild(0).transform.position.z + 1f;
        }
        
        Transform finish = Instantiate(_finish, _worldParent).transform;
        finish.transform.position = new Vector3(0, -.4f, FinishZPos);
        _50procLevel.transform.position = new Vector3(0, 0, finish.transform.position.z / 2);
        _platform.transform.position = new Vector3 (0, 0, FinishZPos);

        //genFinish
        _presetsL.Add(Instantiate(_wall, _worldParent));
        _presetsL[_presetsL.Count - 1].transform.position = _presetsL[_presetsL.Count - 2].transform.GetChild(0).transform.position;
        _presetsL[_presetsL.Count - 1].transform.localScale = new Vector3(1, 30, 1);

        _presetsR.Add(Instantiate(_wall, _worldParent));
        _presetsR[_presetsR.Count - 1].transform.position = _presetsR[_presetsR.Count - 2].transform.GetChild(0).transform.position;
        _presetsR[_presetsR.Count - 1].transform.localScale = new Vector3(1, 30, 1);
        _presetsR[_presetsR.Count - 1].transform.GetChild(1).gameObject.layer = 9;


        for (int i = 0; i < 7; i++)
        {
            float z = Mathf.Lerp(_presetsL[1].transform.GetChild(0).position.z, _presetsL[_presetsL.Count - 5].transform.GetChild(0).position.z, (i + 1) / 7f);
            Instantiate(_pin, _worldParent).transform.position = new Vector3(Random.Range(-_posWall / _worldGenData.OffsetPosPinX, _posWall / _worldGenData.OffsetPosPinX), 0, Random.Range(z - _posWall / _worldGenData.OffsetPosPinX, z + _posWall / _worldGenData.OffsetPosPinX));
        }

    }
	
    void GenPreset(ref List<GameObject> listGo)
    {
        bool isRight = false;
        int nowOffsetSpikes = Random.Range(_worldGenData.OffsetSpikesMin, _worldGenData.OffsetSpikesMax + 1);
        float nowOffsetWall = Random.Range(_worldGenData.OffsetWallMin, _worldGenData.OffsetWallMax + 1);

        listGo.Add(Instantiate(_wall, _worldParent));
        if (listGo == _presetsR)
        {
            isRight = true;
            for (int i = 0; i < listGo[listGo.Count - 1].transform.childCount; i++)
                listGo[listGo.Count - 1].transform.GetChild(i).gameObject.layer = 9;
        }
        listGo[listGo.Count - 1].transform.position = listGo[listGo.Count - 2].transform.GetChild(0).position;
        listGo[listGo.Count - 1].transform.localScale = new Vector3(1, nowOffsetWall * _oneStepWall, 1);
        for (int i = 0; i < nowOffsetSpikes; i ++)
        {
            listGo.Add(Instantiate(_spikes, _worldParent));
            if (isRight)
                for (int g = 0; g < listGo[listGo.Count - 1].transform.childCount; g++)
                    listGo[listGo.Count - 1].transform.GetChild(g).gameObject.layer = 9;
            listGo[listGo.Count - 1].transform.position = listGo[listGo.Count - 2].transform.GetChild(0).position;
            if (listGo[listGo.Count - 1].transform.localPosition.x > 0)
                listGo[listGo.Count - 1].transform.localScale = new Vector3(1, -1, 1);
        }
    }

    private void OnApplicationQuit()
    {
        _worldGenData.ResetWorldData();
        _palette.ResetIndexColor();
    }
}
