﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AlphaMeshText : MonoBehaviour {

    [SerializeField] float _wait;
    [SerializeField] float _speed;
    [SerializeField] Color _color1;
    [SerializeField] Color _color2;
    float _nowT = 0;
    TextMeshPro _textMesh;

    void Start()
    {
        _textMesh = GetComponent<TextMeshPro>();
    }

    private void Update()
    {
        if(_wait > 0)
        {
            _wait -= Time.deltaTime;
            goto wait;
        }

        _nowT += Time.deltaTime * _speed;
        _textMesh.color = Color.Lerp(_color1, _color2, _nowT);
        if (_nowT >= 1)
            Destroy(transform.parent.gameObject);
        
        wait:;
    }
}
