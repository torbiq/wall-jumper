﻿namespace myEnums
{
    public enum GameOver {win, lose}
    public enum TypeBlock {wall, spike}
    public enum Side {left, right}
}
