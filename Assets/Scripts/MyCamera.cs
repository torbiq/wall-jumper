﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour
{
    [SerializeField] Transform _ball;
    Vector3 _currentVelocity;
    [SerializeField] float _offsetDist = 6.75f;
    [SerializeField] float speedCam = 1f;

    Vector3 lastPos;
    Vector3 deltaPos;

    bool _isResetPos;
    public bool IsResetPos { set { _isResetPos = value; } }
    float _lastPosZ;

    private void Awake()
    {
        Application.targetFrameRate = 60;
    }

    void LateUpdate()
    {
        if (!_isResetPos)
            transform.position = Vector3.SmoothDamp(new Vector3(0, transform.position.y, transform.position.z), new Vector3(0, transform.position.y, _ball.position.z - _offsetDist), ref _currentVelocity, Time.deltaTime * speedCam);
        else
        {
            _lastPosZ = Mathf.Abs(transform.position.z);
            transform.position = Vector3.SmoothDamp(new Vector3(0, transform.position.y, transform.position.z), new Vector3(0, transform.position.y, _ball.position.z - _offsetDist), ref _currentVelocity,  .2f);
            if (Mathf.Abs(transform.position.z) - _lastPosZ > .1f)
                IsResetPos = false;
        }
    }

}
