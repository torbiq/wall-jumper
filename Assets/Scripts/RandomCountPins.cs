﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCountPins : MonoBehaviour {

    [SerializeField] GameObject[] _pins;

	void Start ()
    {
        for (int i = Random.Range(1, _pins.Length + 1); i < _pins.Length; i++)
            _pins[i].SetActive(false);
	}
}
