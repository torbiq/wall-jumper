﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScoreAdd : MonoBehaviour
{

	void Start ()
    {
        transform.localScale = new Vector3(0, 0, 0);
        transform.DOScale(new Vector3(1.4f, 1.4f, 1.4f), .2f).OnComplete(() => transform.DOScale(new Vector3(1, 1, 1), .12f).OnComplete(Effects));
        //DOVirtual.DelayedCall(.4f, Effects);
	}
	
	void Effects()
    {
        transform.DOMoveY(transform.parent.Find("PivotAddTextScore").transform.position.y, 1f);
        Color c = GetComponent<Text>().color;
        c.a = 0;
        GetComponent<Text>().DOColor(c, .6f).OnComplete(() => Destroy(gameObject));
    }
}
