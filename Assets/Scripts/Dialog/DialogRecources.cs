﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogRecources : MonoBehaviour {

	static Transform _canvasForDialog;
    static GameObject _dialogGo;

    public static Transform CanvasForDialog { get { return _canvasForDialog; } }
    public static GameObject DialogGo { get { return _dialogGo; } }

    void Awake()
    {
        _canvasForDialog = GameObject.FindGameObjectWithTag("Canvas").transform;
        _dialogGo = Resources.Load<GameObject>("Dialog");
    }
}
