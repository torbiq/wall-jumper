﻿using UnityEngine;
using UnityEngine.UI;

public class ElemetsDialog : MonoBehaviour
{
    [SerializeField] protected Text textTitle;
    [SerializeField] protected Text textDescr;
    [SerializeField] protected Button buttOk;
    [SerializeField] protected Button buttNo;

    public static void AddCloseDialog(Button butt, GameObject dialog)
    {
        butt.onClick.AddListener(() => Destroy(dialog));
    }
    public static void ChangeTextButt(string name, Button butt)
    {
        butt.GetComponentInChildren<Text>().text = name;
    }
    public static void ChangeText(Text text, string mes)
    {
        text.text = mes;
    }
}
