﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Dialog: ElemetsDialog
{
    public static void CreateMesDialog(string title, string descr, string yes, string no)
    {
        Dialog dialog = Instantiate(DialogRecources.DialogGo, DialogRecources.CanvasForDialog).GetComponent<Dialog>();
        dialog.transform.SetAsLastSibling();

        ChangeText(dialog.textTitle, title);
        ChangeText(dialog.textDescr, descr);

        ChangeTextButt(yes, dialog.buttOk);
        AddCloseDialog(dialog.buttOk, dialog.gameObject);

        ChangeTextButt(no, dialog.buttNo);
        AddCloseDialog(dialog.buttNo, dialog.gameObject);
    }

    public static void CreateMesDialog(string title, string descr, string yes, UnityAction yesFunc, string no, UnityAction noFun)
    {
        Dialog dialog = Instantiate(DialogRecources.DialogGo, DialogRecources.CanvasForDialog).GetComponent<Dialog>();
        dialog.transform.SetAsLastSibling();

        ChangeText(dialog.textTitle, title);
        ChangeText(dialog.textDescr, descr);
        
        ChangeTextButt(yes, dialog.buttOk);
        if (yesFunc != null)
            dialog.buttOk.onClick.AddListener(yesFunc);
        AddCloseDialog(dialog.buttOk, dialog.gameObject);

        ChangeTextButt(no, dialog.buttNo);
        if (noFun != null)
            dialog.buttNo.onClick.AddListener(noFun);
        AddCloseDialog(dialog.buttNo, dialog.gameObject);
    }
}
