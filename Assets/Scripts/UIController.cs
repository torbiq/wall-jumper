﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

   
    MyCamera _myCamera;
    WorldGenData _worldGenData;
    WorldGen _worldGen;

    [SerializeField] Ball _ball;

    [SerializeField] Image _progressBarImage;
    [SerializeField] Text _nowLvl;
    [SerializeField] Text _nextLvl;
    [SerializeField] float _speedProgressBar = 1;

    [SerializeField] Text _textPassedLevel;
    [SerializeField] Text _textFailedLevel;
    [SerializeField] Text _textFaliledPercent;

    [SerializeField] Image[] _needColorChange;

    [Space(10)]
    int _countNowGetPin;
    int _maxScore = 12;
    [SerializeField] Transform _strikeText;

    [Space(10)]
    [SerializeField] Image[] _imgCounterPin;

    [SerializeField] Text _scoreText;

    [SerializeField] Transform _goLastDeathLine;

    [SerializeField] Transform _containerSubTextScore;
    [SerializeField] GameObject _subTextScore;

    void Awake()
    {
        _myCamera = Camera.main.GetComponent<MyCamera>();
        _worldGenData = Camera.main.GetComponent<WorldGen>().WorldGenData;
        _worldGen = GetComponent<WorldGen>();
        _maxScore = _imgCounterPin.Length;
        _progressBarImage.fillAmount = 0f;
        _scoreText.text = "0";
        _nowLvl.text = _worldGenData.NowLvl.ToString();
        _nextLvl.text = (_worldGenData.NowLvl + 1).ToString();
    }
    private void Start()
    {
        if (_needColorChange.Length > 0)
            foreach (Image img in _needColorChange)
                img.color = _worldGen.NowPalette.GetNowWallColor;
    }

    void Update()
    {
        if (_progressBarImage.fillAmount < 1)
            _progressBarImage.fillAmount = Mathf.MoveTowards(_progressBarImage.fillAmount, _ball.GetFillAmount() / 100, Time.deltaTime * _speedProgressBar);
    }

    public void LoadLvl()
    {
        SceneManager.LoadScene(0);
    }
    
    public void RestartLevel()
    {
        _goLastDeathLine.position = new Vector3(0, 0, _ball.transform.position.z);
        _scoreText.text = "0";
        _myCamera.IsResetPos = true;
        _worldGen.ResetDownedPins();
        _ball.ResetBall();
    }

    public void IncrementCountPin()
    {
        _countNowGetPin++;
        _imgCounterPin[_countNowGetPin - 1].DOFade(1, .2f);
        _imgCounterPin[_countNowGetPin - 1].transform.DOPunchScale(new Vector3(.15f, .15f, .15f), .8f);

        if (_countNowGetPin == _maxScore)
        {
            StrikeText();
        }
    }

    public void AddNewScore(int scoreTextAdd, Color color)
    {
        Text t =  Instantiate(_subTextScore, _containerSubTextScore).GetComponent<Text>();
        t.text = "+" + scoreTextAdd;
        t.color = color;
    }

    public void StrikeText()
    {
        _strikeText.DOKill();
        _strikeText.gameObject.SetActive(true);
        _strikeText.DOScale(Vector3.one, .4f).OnComplete(() => _strikeText.DOScale(new Vector3(.65f, .65f, .65f), .4f).SetLoops(6, LoopType.Yoyo).SetEase(Ease.Linear).OnComplete(() => _strikeText.DOScale(Vector3.zero, .4f).OnComplete(() => _strikeText.gameObject.SetActive(false))));
    }

    public void UpdateInfoGameOverPanel(myEnums.GameOver typeGameOver)
    {
        if (typeGameOver == myEnums.GameOver.lose)
        {
            _textFailedLevel.text = "Level " + _worldGenData.NowLvl;
            _textFaliledPercent.text = (int)(_progressBarImage.fillAmount * 100) + "% completed";
        }
        else
        {
            _textPassedLevel.text = "Level " + _worldGenData.NowLvl + "\nPassed!";
        }
    }

    public void UpdateScoreText(int nowScore)
    {
        _scoreText.text = nowScore.ToString();
        _scoreText.transform.DOKill();
        _scoreText.transform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), .15f).OnComplete(() => _scoreText.transform.DOScale(Vector3.one, .15f));
    }
}
