﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyCallback : MonoBehaviour
{
    [SerializeField] bool _isDestroyParent;
    public static bool IsNeedResetParticle;

    void Update()
    {
        if(IsNeedResetParticle)
        {
            if (_isDestroyParent)
                Destroy(transform.parent.gameObject);
            else
                Destroy(gameObject);
        }
            
    }

    void OnParticleSystemStopped()
    {
        if (_isDestroyParent)
            Destroy(transform.parent.gameObject);
        else
            Destroy(gameObject);
    }
}
