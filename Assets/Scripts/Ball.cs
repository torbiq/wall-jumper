﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using myEnums;
using TMPro;
using UnityEngine.Rendering.PostProcessing;

public class Ball : MonoBehaviour
{
    [SerializeField] PostProcessProfile _postProcessProfile;

    [SerializeField] TextsPin _textsPin;
    Palette _nowActivePalette;
    WorldGen _worldGen;
    WorldGenData _worldGenData;

    [SerializeField] ParticleSystem _particleTrail;

    [SerializeField] GameObject _particleFireWall;
    [SerializeField] GameObject _particleWall;
    [SerializeField] GameObject _particleFireworks;
    [SerializeField] GameObject _particleExplPin;
    [SerializeField] UIController _uiController;
    [SerializeField] ParticleSystem _berserkerParticle;
    [SerializeField] ParticleSystem _berserkerParticle2;
    TrailRenderer _ballTrailRenderer;

    [SerializeField] Color _color2;
    Material _ballMat;

    [SerializeField] float _speed = 3.2f;
    float _zTranslate = 0;

    [SerializeField] float _speedUpIntensity = .3f;
    [SerializeField] float _speedDownIntensity = .7f;
    [SerializeField] float _speedUp = 1;
    [SerializeField] float _speedUpInBerserkMode = 2f;
    float _timerToResetBerserker = 0;
    [SerializeField] float _maxTimeBeforResetBerserker = 2f;
    [SerializeField] float _needSpeedForBerserker = 2f;
    bool _isBerserker;
    bool _isNeedResetBerserker;

    [SerializeField] float posW_all = 1.7f;
    [SerializeField] float _speedMoveDown = 3;
    float _direction = -1;

    float _startZPos;

    float _timerChangeDir = 0;
    bool _isNeedCheckPos = true;
    bool _isMoveBall = true;
    bool _wasMouseHold;

    [SerializeField] GameObject _losePanel;
    [SerializeField] GameObject _winPanel;
    [SerializeField] ParticleSystem[] _particleWin;

    RaycastHit _hit;
    Vector3 _lastBallPos;

    int _score;
    int _nowPinStrike;
    int _nowCombo = 1;
    List<GameObject> _wallAlreadyCombo = new List<GameObject>();
    [SerializeField] float _maxTimerStrike = 1f;
    float _timerStrike;
    bool _isStrikeGroup;

    List<GameObject> _wallComplite = new List<GameObject>();

    private void Awake()
    {
        _worldGen = Camera.main.GetComponent<WorldGen>();
        _worldGenData = _worldGen.WorldGenData;
        _nowActivePalette = _worldGen.NowPalette;

        _ballMat = GetComponent<MeshRenderer>().material;
        _startZPos = transform.position.z;
        _ballTrailRenderer = GetComponent<TrailRenderer>();
        //_matTrail.color = _trailColor1;
        _postProcessProfile.GetSetting<Bloom>().enabled.value = false;
    }

    private void Update()
    {
        #region timers
        if (_timerToResetBerserker > 0)
        {
            _timerToResetBerserker -= Time.deltaTime;
            if (_timerToResetBerserker <= 0)
            {
                _isBerserker = false;
                _speedUp = _needSpeedForBerserker;
                _isNeedResetBerserker = true;
                _berserkerParticle.Stop();
                _berserkerParticle2.Stop();
            }
        }

        if (_timerChangeDir > 0)
        {
            _timerChangeDir -= Time.deltaTime;
            if (_timerChangeDir <= 0)
            {
                _direction *= -1;
                _timerChangeDir = 0;
            }
        }

        if (_isStrikeGroup)
        {
            _timerStrike -= Time.deltaTime;
            if (_timerStrike <= 0)
            {
                //_nowPinStrike = 0;
                _score += _nowPinStrike * 7;
                CreateTextGroupStrike();
                _uiController.UpdateScoreText(_score);
                _uiController.AddNewScore(_nowPinStrike * 7, Color.green);
                _isStrikeGroup = false;
                _timerStrike = 0;
                _nowPinStrike = 0;

                _nowCombo = 1;
               // print("nice");
            }
        }
        
        if (_isNeedResetBerserker)
        {
            _speedUp -= Time.deltaTime * _speedDownIntensity;
            float tColor = (_speedUp - 1) / (_needSpeedForBerserker - 1);
            _ballMat.color = Color.Lerp(Color.white, Color.red, tColor);
            _particleTrail.startColor = Color.Lerp(Color.white, _color2, tColor);
            //_ballTrailRenderer.startColor = Color.Lerp(Color.white, Color.red, tColor);
            //_ballTrailRenderer.endColor = Color.Lerp(Color.white, Color.yellow, tColor);
            if (_speedUp <= 1)
            {
                _postProcessProfile.GetSetting<Bloom>().enabled.value = false;
                _isNeedResetBerserker = false;
                _speedUp = 1;
            }
        }
        #endregion



        if (_isMoveBall)
        {
            if (transform.position.z >= WorldGen.FinishZPos)
                GameOver(myEnums.GameOver.win);

            if (Input.GetMouseButtonUp(0))
            {
                if (_wasMouseHold)
                    _wasMouseHold = false;

                if (_isBerserker)
                    _timerToResetBerserker = _maxTimeBeforResetBerserker;
                else
                {
                    _isNeedResetBerserker = true;
                }

                _zTranslate = 0;
            }

            if(Input.GetMouseButtonDown(0))
            {
                _isNeedResetBerserker = false;


                if(_isBerserker)
                    _timerToResetBerserker = 0;
            }

            if (Input.GetMouseButton(0))
            {
                if (!_wasMouseHold)
                    _wasMouseHold = true;



                
                if (!_isBerserker)
                {
                    if (_isNeedResetBerserker)
                        _isNeedResetBerserker = false;
                    _speedUp += Time.deltaTime * _speedUpIntensity;
                    float tColor = (_speedUp - 1) / (_needSpeedForBerserker - 1);
                    _ballMat.color = Color.Lerp(Color.white, Color.red, tColor);
                    _particleTrail.startColor = Color.Lerp(Color.white, _color2, tColor);
                    //_ballTrailRenderer.startColor = Color.Lerp(Color.white, Color.red, tColor);
                    //_ballTrailRenderer.endColor = Color.Lerp(Color.white, Color.yellow, tColor);
                    if (_speedUp >= _needSpeedForBerserker)
                    {
                        print("test berserker");
                        _speedUp = _speedUpInBerserkMode;
                        _postProcessProfile.GetSetting<Bloom>().enabled.value = true;
                        _isBerserker = true;
                        _berserkerParticle.Play();
                        _berserkerParticle2.Play();
                    }
                }
                _zTranslate = Time.deltaTime * _speedMoveDown * _speedUp;
            }                


            #region rotationParticle
            var delta = transform.position - _lastBallPos;
            var direction = delta.normalized;

            if (_isBerserker)
            {
                 _berserkerParticle.transform.LookAt(_berserkerParticle.transform.position - direction, Vector3.up);
               // _berserkerParticle.transform.LookAt(_ballTrailRenderer.GetPosition(0));
            }
            #endregion

            _lastBallPos = transform.position;
            transform.Translate(_direction * Time.deltaTime * _speed * _speedUp, 0, _zTranslate);
        }
    }

    void LateUpdate()
    {
        if(_isMoveBall)
        {        
            if (transform.position.z >= WorldGen.FinishZPos)
                GameOver(myEnums.GameOver.win);

            if (_isNeedCheckPos)
            {


                if (Physics.Raycast(transform.position, transform.TransformDirection(GetDirectionRayCheck()), out _hit, transform.localScale.x / 2) && _hit.collider != null)
                {
                    if (_hit.transform.tag == "WallNorm")
                    {
                        BouncesBall();
                        if (_isBerserker)
                        {
                            _score += 5;
                            _uiController.AddNewScore(5, Color.red);

                            Instantiate(_particleFireWall, new Vector3(Mathf.Abs(posW_all - .25f) * _direction, _hit.point.y, _hit.point.z), Quaternion.identity);
                        }
                        else
                            Instantiate(_particleWall, new Vector3(Mathf.Abs(posW_all - .2f) * _direction, _hit.point.y, _hit.point.z), Quaternion.identity);


                        if(!_wallComplite.Contains(_hit.transform.gameObject))
                        {
                            _wallComplite.Add(_hit.transform.gameObject);
                            if(!_isBerserker)
                            {
                                _score += 3;
                                _uiController.AddNewScore(3, Color.white);
                            }
                            else
                            {
                                _score += 5;
                                _uiController.AddNewScore(5, Color.red);
                            }
                            _uiController.UpdateScoreText(_score); 
                        }

                        goto SkipCheckGameOver;
                    }
                }


                if (Mathf.Abs(transform.position.x) >= Mathf.Abs(posW_all) + .2f)
                {
                    if (_isBerserker)
                    {
                        BouncesBall();
                        ResetBerserker();
                    }
                    else
                        GameOver(myEnums.GameOver.lose);
                }

                SkipCheckGameOver:;
            }
        }

    }

    void BouncesBall()
    {
        TapticPlugin.TapticManager.Impact(TapticPlugin.ImpactFeedback.Light);
        _isNeedCheckPos = false;
        transform.DOKill();
        transform.DOPunchScale(new Vector3(-0.3f, 0.3f, 0), 0.25f / _speedUp, 0, 0);
        _timerChangeDir = .065f / _speedUp;
        // .06 | 1  s
        DOVirtual.DelayedCall(_timerChangeDir * 2 + .3f, () => _isNeedCheckPos = true, false);
    }
    
    void ResetBerserker()
    {
        _postProcessProfile.GetSetting<Bloom>().enabled.value = false;
        _timerToResetBerserker = 0;
        _wallComplite.Clear();
        _speedUp = 1;
        _zTranslate = 0;
        _isBerserker = false;
        _isNeedResetBerserker = false;
        _ballMat.color = Color.white;
       // _ballMat.DOColor(Color.white, .1f);
        _particleTrail.startColor = Color.white;
        _berserkerParticle.Stop();
        _berserkerParticle2.Stop();
        //_matTrail.color = _trailColor1;
        //_ballTrailRenderer.startColor = Color.white;
        //_ballTrailRenderer.endColor = Color.white;
    }

    public void ResetBall()
    {
        _score = 0;
        ParticleDestroyCallback.IsNeedResetParticle = true;
        DOVirtual.DelayedCall(.1f, () => ParticleDestroyCallback.IsNeedResetParticle = false);
        _zTranslate = 0;
        _particleTrail.Stop();
        transform.position = new Vector3(0, 0, 0);
        _particleTrail.Play();
        transform.localScale = new Vector3(0.70343f, 0.70343f, 0.70343f);
        _losePanel.SetActive(false);
        _isNeedCheckPos = true;
        _isMoveBall = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pin")
        {
            _worldGen.DownedPins = other.gameObject;
            other.gameObject.SetActive(false);

            //if (!_isStrikeGroup && other.transform.parent.childCount < 3)
            //    goto skipCheckGroupStrike;

            _isStrikeGroup = true;
            _timerStrike = _maxTimerStrike; 

            _nowPinStrike++;
            //if(_isStrikeGroup && _nowPinStrike == 3)
            //{
            //    _isStrikeGroup = false;
            //    _timerStrike = 0;
            //    _nowPinStrike = 0;
            //    CreateTextGroupStrike(TypeGroupStrike.strike);
            //    _nowCombo++;
            //    _uiController.IncrementComboText(_nowCombo);
            //    //   _uiController.StrikeText();
            //}

            //skipCheckGroupStrike:;

            TapticPlugin.TapticManager.Impact(TapticPlugin.ImpactFeedback.Medium);
            if (!_isBerserker)
                Instantiate(_particleFireworks, other.transform.position, Quaternion.identity);
            else
                Instantiate(_particleExplPin, other.transform.position, Quaternion.identity);
            //Destroy(other.gameObject, .05f);
            //_uiController.IncrementCountPin();
        }
    }

    Vector3 GetDirectionRayCheck()
    {
        if (transform.position.x < 0)
            return Vector3.left;
        else
            return Vector3.right;
    }

    void CreateTextGroupStrike()
    {
        GameObject go = Instantiate(_textsPin.GoTextSpawn);
        go.transform.localScale = new Vector3(0, 0, 0);
        go.transform.GetChild(0).GetComponent<TextMeshPro>().text = _textsPin.TextGroup[_nowPinStrike - 1];
        go.transform.DOScale(new Vector3(1, 1, 1), .6f);
        go.transform.position = new Vector3(0, 0, transform.position.z);
        go.transform.DOMoveY(1.5f, 1f);
        //go.transform.GetChild(0).GetComponent<TextMesh>().color.
    }

    void GameOver(GameOver gameOver)
    {
        _isMoveBall = false;
        _isNeedCheckPos = false;
        transform.DOKill();
        _uiController.UpdateInfoGameOverPanel(gameOver);
        ResetBerserker();
        if (gameOver == myEnums.GameOver.lose)
        {
            TapticPlugin.TapticManager.Impact(TapticPlugin.ImpactFeedback.Heavy);
            print("Lose");
            _losePanel.SetActive(true);
        }
        else
        {
            _worldGenData.NextLevel();
            _nowActivePalette.NowIndexColor++;

            print("Win");
            _winPanel.SetActive(true);

            foreach (ParticleSystem go in _particleWin)
                go.Play();
        }
    }

    public float GetFillAmount()
    {
        return (transform.position.z - _startZPos) / (WorldGen.FinishZPos- _startZPos) * 100;
    }
}
