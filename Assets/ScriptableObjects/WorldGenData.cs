﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WorldGenData", menuName = "My ScriptableObj/New WorldGenData")]
public class WorldGenData : ScriptableObject
{
    int _nowLvl = 1;
    public int NowLvl { get { return _nowLvl; } /*set { _nowLvl = value; } */}

    [SerializeField] int _maxLvl = 50;

    int _sizeWorld;
    [Header("Size world")]
    [SerializeField] [LabelOverride("Min")] int _minSizeWorld = 12;
    [SerializeField] [LabelOverride("Max")] int _maxSizeWorld = 25;

    int _offsetSpikesMin;
    [Header("Offset count spikes min")]
    [SerializeField] [LabelOverride("Min")] int _minOffsetCountSpikesMin = 1;
    [SerializeField] [LabelOverride("Max")] int _maxOffsetCountSpikesMin = 2;

    int _offsetSpikesMax;
    [Header("Offset count spikes max")]
    [SerializeField] [LabelOverride("Min")] int _minOffsetSpikesMax = 3;
    [SerializeField] [LabelOverride("Max")] int _maxOffsetSpikesMax = 5;

    float _offsetWallMin;
    [Header("Offset wall min")]
    [SerializeField] [LabelOverride("Min")] float _minOffsetWallMin = 4.7f;
    [SerializeField] [LabelOverride("Max")] float _maxOffsetWallMin = 3.2f;

    float _offsetWallMax;
    [Header("Offset wall max")]
    [SerializeField] [LabelOverride("Min")] float _minOffsetWallMax = 6.7f;
    [SerializeField] [LabelOverride("Max")] float _maxOffsetWallMax = 5f;

    float _offsetPosPinX;
    [Header("Offset position pins X")]
    [SerializeField] [LabelOverride("Min")] float _minOffsetPosPinX = 2.7f;
    [SerializeField] [LabelOverride("Max")] float _maxOffsetPosPinX = 1.8f;

    float _offsetPosPinZ;
    [Header("Offset position pins Z")]
    [SerializeField] [LabelOverride("Min")] float _minOffsetPosPinZ = 4f;
    [SerializeField] [LabelOverride("Max")] float _maxOffsetPosPinZ = .5f;

    public int SizeWorld { get { return _sizeWorld; } }
    public int OffsetSpikesMin { get { return _offsetSpikesMin; } }
    public int OffsetSpikesMax { get { return _offsetSpikesMax; } }
    public float OffsetWallMin { get { return _offsetWallMin; } }
    public float OffsetWallMax { get { return _offsetWallMax; } }
    public float OffsetPosPinX { get { return _offsetPosPinX; } }
    public float OffsetPosPinZ { get { return _offsetPosPinZ; } }

    public void NextLevel()
    {
        _nowLvl++;
        SetWorldData();
    }

    public void ResetWorldData()
    {
        _nowLvl = 1;
        SetWorldData();
    }

    void SetWorldData()
    {
        float t = _nowLvl / (float)_maxLvl;
        _sizeWorld = (int)Mathf.Lerp(_minSizeWorld, _maxSizeWorld, t);
        _offsetWallMin = Mathf.Lerp(_minOffsetWallMin, _maxOffsetWallMin, t);
        _offsetWallMax = Mathf.Lerp(_minOffsetWallMax, _maxOffsetWallMax, t);
        _offsetSpikesMin = (int)Mathf.Lerp(_minOffsetCountSpikesMin, _maxOffsetCountSpikesMin, t);
        _offsetSpikesMax = (int)Mathf.Lerp(_minOffsetSpikesMax, _maxOffsetSpikesMax, t);
        _offsetPosPinX = Mathf.Lerp(_minOffsetPosPinX, _maxOffsetPosPinX, t);
        _offsetPosPinZ = Mathf.Lerp(_minOffsetPosPinZ, _maxOffsetPosPinZ, t);
    }
    //public static WorldGenData operator ++(WorldGenData wd)
    //{
    //    Debug.Log("++");
    //    return null;
    //}
}
