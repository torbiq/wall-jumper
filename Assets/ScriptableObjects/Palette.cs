﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Palette", menuName = "My ScriptableObj/New Palette")]
public class Palette : ScriptableObject
{
    int _nowIndexColor;
    public int NowIndexColor { get { return _nowIndexColor; } set { _nowIndexColor = value % _wallColors.Length; } }

    [SerializeField] Color[] _wallColors;
    [SerializeField] Color[] _platformColors;
    [SerializeField] Color[] _skyColors;

    public Color GetNowWallColor { get { return _wallColors[_nowIndexColor]; } }
    public Color GetNowPlatformColor { get { return _platformColors[_nowIndexColor]; } }
    public Color GetNowSkyColors { get { return _skyColors[_nowIndexColor]; } }

    public void ResetIndexColor()
    {
        _nowIndexColor = 0;
    }
}
