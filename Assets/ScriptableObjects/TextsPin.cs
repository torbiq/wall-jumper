﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TextsPin", menuName = "My ScriptableObj/New TextsPin")]
public class TextsPin : ScriptableObject {

    [SerializeField] string[] _textGroup;
    [SerializeField] GameObject _goTextSpawn;

    public string[] TextGroup { get { return _textGroup; } }
    public GameObject GoTextSpawn { get { return _goTextSpawn; } }
}
